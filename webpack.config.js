var path = require('path');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var webpack = require("webpack");

module.exports = {
    entry: './app.js',
    output: {
        path: '/home/deus/WebstormProjects/nodeCompilied/views/compiled/',
        filename: 'bundle.js'
    },
     resolve: {
    extensions: ['', '.js', '.jsx']
      },
      module: {
        loaders: [
          {
            test: /\.jsx?$/,
            loader: 'babel',
            exclude: /node_modules/,
            query: {
              cacheDirectory: true,
              presets: ['react', 'es2015']
            }
          },
          {
            test: /\.scss$/,
            loader: ExtractTextPlugin.extract("css-loader!sass-loader")
          },
          { test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "url-loader?limit=10000&minetype=application/font-woff" },
          { test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "file-loader" },
          { test: /\.json$/, loader: "json-loader" }
        ]
      },
      plugins: [
        new ExtractTextPlugin("bundle.css")
      ]
};