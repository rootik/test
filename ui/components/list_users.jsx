import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';

class ListUsers extends Model{
    constructor() {
        super({
            model:['Games'],
            stores: ['Result']
        });

        this.state = {};
        this.storeResult = this.store['Result'];

        this.storeResult.on("change", ()=>{
            this.setState({
                data:this.storeResult._get()
            });
        });
    }

    componentWillMount(){}

    getPlayers() {
        if(this.state.data){
            var data = this.state.data[0].results, players = [], i = 0;

            for (var prop in data) {
                if(data.hasOwnProperty(prop)) players[i] = data[prop];
                i++;
            } 
            
            players.length = (Object.keys(players)).length;
            const table = players.map((player)=>{
                return <TableRow>
                            <TableRowColumn className='columns'><img src={player.photo}/></TableRowColumn>
                            <TableRowColumn className='columns'>{player.email}</TableRowColumn>
                            <TableRowColumn className='columns'>{Math.round(player.result)}</TableRowColumn>
                        </TableRow>;
            });

            return table;
        }
    }

    render() {
        return (
            <Table className='tables'> 
                <TableHeader>
                    <TableRow>
                        <TableHeaderColumn>Photo</TableHeaderColumn>
                        <TableHeaderColumn>Name</TableHeaderColumn>
                        <TableHeaderColumn>Result</TableHeaderColumn>
                    </TableRow>
                </TableHeader>
                <TableBody>
                    
                    {this.getPlayers()}
                </TableBody>
            </Table>
        );
    }
}

export default ListUsers;