import IconMenu from 'material-ui/IconMenu';
import IconButton from 'material-ui/IconButton';
import FontIcon from 'material-ui/FontIcon';
import NavigationExpandMoreIcon from 'material-ui/svg-icons/navigation/expand-more';
import MenuItem from 'material-ui/MenuItem';
import DropDownMenu from 'material-ui/DropDownMenu';
import RaisedButton from 'material-ui/RaisedButton';
import {Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle} from 'material-ui/Toolbar';

class Header extends Model{
    constructor() {
        super({
            models: ['Users', 'Games'],
            stores: ['Users']    
        });
        this.state = {data:{}};
        this.modelUsers = this.model['Users'];
        this.storeUsers = this.store['Users'];
        this.modelGames = this.model['Games'];
        
        this.storeUsers.on("change", ()=>{
            this.setState({
                data:this.storeUsers._getUsers()
            });
        });

        this.modelUsers.getData();
    }

    render() {
        if(localStorage.getItem("check")==="true" && document.getElementById("clickGame")) this.modelGames.setButton();
        return (
            <header>
                 <Toolbar>
                    <ToolbarGroup firstChild={true}>
                        <img src={this.state.data.photo} alt=""/>
                        <ToolbarTitle text={this.state.data.email} className="title"/>
                    </ToolbarGroup>
                    <ToolbarGroup>
                    <ToolbarTitle text="Options" />
                    <FontIcon className="muidocs-icon-custom-sort" />
                    <ToolbarSeparator />
                    <RaisedButton label="Start new Game" primary={true} id='clickGame' onClick={this.modelGames.startGame} />
                    <IconMenu
                        iconButtonElement={
                        <IconButton touch={true}>
                            <NavigationExpandMoreIcon />
                        </IconButton>
                        }
                    >
                        <MenuItem primaryText="Download" />
                        <MenuItem primaryText="Exit" onClick={this.modelUsers.logout} />
                    </IconMenu>
                    </ToolbarGroup>
                </Toolbar>
            </header>
        );
    }
}

export default Header;