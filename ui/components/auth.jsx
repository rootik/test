import FloatingActionButton from 'material-ui/FloatingActionButton';
import FontIcon from 'material-ui/FontIcon';
import baseTheme from 'material-ui/styles/baseThemes/lightBaseTheme';
import getMuiTheme from 'material-ui/styles/getMuiTheme';

class Auth extends Model {
    constructor() {
        super({
            models: ['Users'],
            stores: ['Users']  
        });
        this.state = {};
        this.store = this.store['Users'];
        this.model = this.model['Users'];

        this.store.on("change", ()=>{
            this.setState({
                link:this.store._get()[1]
            });
        });
    }

    getChildContext() {
        return { muiTheme: getMuiTheme(baseTheme) };
    }

    componentWillMount(){
        this.model.getLink();
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-xs-12">
                        <div className="auth">
                            <div className="title">
                                Sign in with Google 
                            </div>
                            <div className="auth_block text-lg-center">
                                <FloatingActionButton href={this.state.link} ><i className="fa fa-google-plus" aria-hidden="true"></i></FloatingActionButton>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

Auth.childContextTypes = {
    muiTheme: React.PropTypes.object.isRequired,
};

module.exports = Auth;