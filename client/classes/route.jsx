import React from 'react';
import { Router, Route, Link, browserHistory } from 'react-router';

import App from "../../ui/components/app";
import Games from "../../ui/components/games";

module.exports = class Route extends React.Component {
    render() {
        return (
            <Router history={browserHistory}>
                <Route path="/" component={App}>
                <Route path="/games/" component={Games}/>
                <Route path="*" component={App}/></Route>
            </Router>
        );
    }
}