class StoreResult extends Store{
    constructor() {
        super();
        this._items = [];

        this.dispatcher.register(this.handleActions.bind(this));
        global.dispatcher = this.dispatcher;
    }

    _set(item) {
        this._items.push(item);
        this.emit("change");
    }

    _get() {
        return this._items;
    }

    _clearItem(){
        if (this._items.length>0) this._items.length = 0;
    }

    handleActions(action) {
        switch(action.type) {
            case "SET_PLAYERS": { 
                this._clearItem();
                this._set(action.item);
            }
        }
    }
}

const storeResult = new StoreResult;
module.exports = storeResult;