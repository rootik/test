class StoreUsers extends Store{
    constructor() {
        super();
        this._items = [];

        this.dispatcher.register(this.handleActions.bind(this));
        global.dispatcher = this.dispatcher;
    }

    _set(item) {
        this._items.push(item);
        this.emit("change");
    }

    _get() {
        return this._items;
    }

    handleActions(action) {
        switch(action.type) {
            case "SET_USERS": { this._set(action.item) }
        }
    }
}

const usersStore = new StoreUsers;
module.exports = usersStore;